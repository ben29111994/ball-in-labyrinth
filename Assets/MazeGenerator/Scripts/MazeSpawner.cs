﻿using UnityEngine;
using System.Collections;

//<summary>
//Game object, that creates maze and instantiates it in scene
//</summary>
public class MazeSpawner : MonoBehaviour {
	public enum MazeGenerationAlgorithm{
		PureRecursive,
		RecursiveTree,
		RandomTree,
		OldestTree,
		RecursiveDivision,
	}

	public MazeGenerationAlgorithm Algorithm = MazeGenerationAlgorithm.PureRecursive;
	public bool FullRandom = false;
	public int RandomSeed = 12345;
	public GameObject Floor = null;
	public GameObject Wall = null;
	public GameObject Pillar = null;
	public int Rows = 5;
	public int Columns = 5;
	public float CellWidth = 5;
	public float CellHeight = 5;
	public bool AddGaps = true;
	public GameObject GoalPrefab = null;
    bool isSpawnGoal = false;

	private BasicMazeGenerator mMazeGenerator = null;

	void Start () {
		//if (!FullRandom) {
		//	Random.seed = RandomSeed;
		//}
		//switch (Algorithm) {
		//case MazeGenerationAlgorithm.PureRecursive:
		//	mMazeGenerator = new RecursiveMazeGenerator (Rows, Columns);
		//	break;
		//case MazeGenerationAlgorithm.RecursiveTree:
		//	mMazeGenerator = new RecursiveTreeMazeGenerator (Rows, Columns);
		//	break;
		//case MazeGenerationAlgorithm.RandomTree:
		//	mMazeGenerator = new RandomTreeMazeGenerator (Rows, Columns);
		//	break;
		//case MazeGenerationAlgorithm.OldestTree:
		//	mMazeGenerator = new OldestTreeMazeGenerator (Rows, Columns);
		//	break;
		//case MazeGenerationAlgorithm.RecursiveDivision:
		//	mMazeGenerator = new DivisionMazeGenerator (Rows, Columns);
		//	break;
		//}
		//mMazeGenerator.GenerateMaze ();
  //      for (int row = 0; row < Rows; row++)
  //      {
  //          for (int column = 0; column < Columns; column++)
  //          {
  //              float x = column * (CellWidth + (AddGaps ? .2f : 0));
  //              float z = row * (CellHeight + (AddGaps ? .2f : 0));
  //              MazeCell cell = mMazeGenerator.GetMazeCell(row, column);
  //              GameObject tmp;
  //              GameObject floor = null;
  //              for (int a = -4; a <= 4; a+=4)
  //              {
  //                  for (int b = -4; b <= 4; b += 4)
  //                  {
  //                      floor = Instantiate(Floor, new Vector3(x + a, 0, z + b), Quaternion.Euler(90, 0, 0)) as GameObject;
  //                      floor.transform.parent = transform;
  //                  }
  //              }
  //              //Debug.Log(x + " " + z);
  //              //var randomSpawn = Random.Range(0, 100);
  //              //float limitPoint = CellWidth * (Rows - 1);
  //              if (cell.WallRight)
  //              {
  //                  //float posX = x + CellWidth / 2 + Wall.transform.position.x;
  //                  //float posZ = z + Wall.transform.position.z;
  //                  //if (posX != -2 && posX != 18 && posZ != -2 && posZ != 18)
  //                  //{
  //                      tmp = Instantiate(Wall, new Vector3(x + CellWidth / 2, 0, z) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;// right
  //                      tmp.transform.parent = transform;
  //                  //}
  //              }
  //              if (cell.WallFront)
  //              {
  //                  //float posX = x + Wall.transform.position.x;
  //                  //float posZ = z + CellHeight / 2 + Wall.transform.position.z;
  //                  //if (posX != -2 && posX != 18 && posZ != -2 && posZ != 18)
  //                  //{
  //                      tmp = Instantiate(Wall, new Vector3(x, 0, z + CellHeight / 2) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;// front
  //                      tmp.transform.parent = transform;
  //                  //}
  //              }
  //              if (cell.WallLeft)
  //              {
  //                  //float posX = x - CellWidth / 2 + Wall.transform.position.x;
  //                  //float posZ = z + Wall.transform.position.z;
  //                  //if (posX != -2 && posX != 18 && posZ != -2 && posZ != 18)
  //                  //{
  //                      tmp = Instantiate(Wall, new Vector3(x - CellWidth / 2, 0, z) + Wall.transform.position, Quaternion.Euler(0, 270, 0)) as GameObject;// left
  //                      tmp.transform.parent = transform;
  //                  //}
  //              }
  //              if (cell.WallBack)
  //              {
  //                  //float posX = x + Wall.transform.position.x;
  //                  //float posZ = z - CellHeight / 2 + Wall.transform.position.z;
  //                  //if (posX != -2 && posX != 18 && posZ != -2 && posZ != 18)
  //                  //{
  //                      tmp = Instantiate(Wall, new Vector3(x, 0, z - CellHeight / 2) + Wall.transform.position, Quaternion.Euler(0, 180, 0)) as GameObject;// back
  //                      tmp.transform.parent = transform;
  //                  //}
  //              }
  //              Vector3 posFloor = floor.transform.position;
  //              Vector3 posBall = transform.GetChild(0).position;
  //              if (cell.IsGoal && GoalPrefab != null && !isSpawnGoal)
  //              {
  //                  isSpawnGoal = true;
  //                  tmp = Instantiate(GoalPrefab, new Vector3(x, 0.21f, z), Quaternion.Euler(0, 0, 0)) as GameObject;
  //                  tmp.transform.parent = transform;
  //              }
  //              else if (posFloor.x >= posBall.x - 4 && posFloor.x <= posBall.x + 4 && posFloor.z >= posBall.z - 4 && posFloor.z <= posBall.z + 3)
  //              {
  //                  Controller.instance.SpawnBall(new Vector3(floor.transform.position.x, floor.transform.position.y + 1, floor.transform.position.z), transform.GetChild(0));
  //              }
  //          }
  //      }
		//if(Pillar != null){
		//	for (int row = 0; row < Rows+1; row++) {
		//		for (int column = 0; column < Columns+1; column++) {
		//			float x = column*(CellWidth+(AddGaps?.2f:0));
		//			float z = row*(CellHeight+(AddGaps?.2f:0));
		//			GameObject tmp = Instantiate(Pillar,new Vector3(x-CellWidth/2,0,z-CellHeight/2),Quaternion.identity) as GameObject;
		//			tmp.transform.parent = transform;
		//		}
		//	}
		//}
  //      var plus = (int.Parse(name));
  //      transform.position = new Vector3(CellWidth * 2 * -1, (plus - 1) * -20, CellWidth * 2 * -1);
  //      Controller.instance.CheckTotalBalls();
  //      //transform.localScale = Vector3.one * plus;
    }
}
