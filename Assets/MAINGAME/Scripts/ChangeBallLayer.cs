﻿using UnityEngine;
using System.Collections;
using MoreMountains.NiceVibrations;
using DG.Tweening;
using GPUInstancer;

public class ChangeBallLayer : MonoBehaviour {

    public int LayerOnEnter; // BallInHole
    public int LayerOnExit;  // BallOnTable
    public GameObject fireWorkPrefab;
    ParticleSystem fireWorkParticle;
    public GameObject explosionPrefab;
    ParticleSystem explosionParticle;
    public GameObject coinEffect;

    private void Start()
    {
        var temp = Instantiate(fireWorkPrefab, transform);
        temp.transform.localPosition = new Vector3(0, -2, 0);
        fireWorkParticle = temp.GetComponent<ParticleSystem>();

        var bombTemp = Instantiate(explosionPrefab);
        bombTemp.transform.parent = transform;
        bombTemp.transform.localPosition = new Vector3(0, 2, 0);
        bombTemp.SetActive(false);
        explosionParticle = bombTemp.GetComponent<ParticleSystem>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            other.transform.DOKill();
            var des = transform.position;
            des = new Vector3(des.x, des.y - 0.5f, des.z);
            other.transform.DOMove(des, 0.25f);
            //other.gameObject.layer = LayerOnEnter;
            other.GetComponent<SphereCollider>().isTrigger = true;
            fireWorkParticle.Play();
            int distance = 0;
            if (Controller.bombPrefab != null)
            {
                distance = (int)Vector3.Distance(other.transform.position, Controller.bombPrefab.transform.position);
            }
            var bonus = 10 - distance;
            if(bonus < 1)
            {
                bonus = 1;
            }
            Controller.instance.Scoring(bonus);
            Controller.instance.PlusEffect(other.transform.position, bonus);
            SoundManager.instance.PlaySoundPitch(SoundManager.instance.ball);
        }
        if(other.gameObject.tag == "Bomb")
        {
            other.transform.DOKill();
            var des = transform.position;
            des = new Vector3(des.x, des.y - 0.5f, des.z);
            other.transform.DOMove(des, 0.25f);
            //other.gameObject.layer = LayerOnEnter;
            other.GetComponent<SphereCollider>().isTrigger = true;
            explosionParticle.transform.parent = null;
            explosionParticle.transform.localScale = Vector3.one * 2;
            explosionParticle.gameObject.SetActive(true);
            explosionParticle.Play();
            Destroy(other.gameObject, 1);
            Controller.instance.Lose();
            //SoundManager.instance.PlaySoundPitch(SoundManager.instance.lose);
        }
        if(other.gameObject.tag == "Coin")
        {
            SoundManager.instance.PlaySound(SoundManager.instance.cash);
            Controller.instance.PlusMoney(1);
            other.transform.DOMoveZ(other.transform.position.z + 2, 0.25f);
            try
            {
                var coinPrefab = Instantiate(coinEffect);
                coinPrefab.transform.SetParent(Controller.instance.canvas.transform);
                coinPrefab.transform.SetAsFirstSibling();
                coinPrefab.transform.localScale = Vector3.zero;
                coinPrefab.transform.position = worldToUISpace(Controller.instance.canvas, transform.position);
                coinPrefab.SetActive(true);
                if (!UnityEngine.iOS.Device.generation.ToString().Contains("5"))
                {
                    MMVibrationManager.Vibrate();
                }
                Destroy(coinPrefab, 2);
                Destroy(other.gameObject, 0.25f);
            }
            catch { }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            //other.gameObject.layer = LayerOnExit;
            if (Controller.state == 1)
            {
                other.transform.parent = Controller.instance.ballManager2.transform;
                other.transform.localPosition = Vector3.zero;
                other.GetComponent<SphereCollider>().isTrigger = false;
                other.transform.DOKill();
                Controller.instance.CheckBallLeft();
            }
            else
            {
                Controller.instance.CheckBallLeft();
                Destroy(other.gameObject);
            }
        }
    }

    public Vector3 worldToUISpace(Canvas parentCanvas, Vector3 worldPos)
    {
        //Convert the world for screen point so that it can be used with ScreenPointToLocalPointInRectangle function
        Vector3 screenPos = Camera.main.WorldToScreenPoint(worldPos);
        Vector2 movePos;

        //Convert the screenpoint to ui rectangle local point
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvas.transform as RectTransform, screenPos, parentCanvas.worldCamera, out movePos);
        //Convert the local point to world point
        return parentCanvas.transform.TransformPoint(movePos);
    }
}