﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour {

	public static SoundManager instance;

	public AudioSource audioSource;
    public AudioClip win, lose, cash, ball;
    float pitchTimeOut = 1;

    void Awake(){
        instance = this;
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (pitchTimeOut > 0)
        {           
            pitchTimeOut -= 0.05f;
        }
        else
        {
            audioSource.pitch = 1;
        }
    }

    public void PlaySound(AudioClip clip)
    {
        audioSource.pitch = 0;
        audioSource.PlayOneShot(clip);
	}

    public void PlaySoundPitch(AudioClip clip)
    {
        pitchTimeOut = 2;
        audioSource.pitch += 0.02f;
        audioSource.PlayOneShot(clip);
    }
}
