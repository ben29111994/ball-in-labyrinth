﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GPUInstancer
{
    public class AddRemoveInstances : MonoBehaviour
    {
        public static AddRemoveInstances instance;
        public List<GPUInstancerPrefab> prefabList = new List<GPUInstancerPrefab>();
        int Count = 0;
        public GPUInstancerPrefabManager prefabManager;

        private Transform parentTransform;
        private int instanceCount;
        public List<GPUInstancerPrefab> instancesList = new List<GPUInstancerPrefab>();
        private string bufferName = "colorBuffer";

        private void OnEnable()
        {
            instance = this;
            Count = prefabList.Count;
        }

        public void InitGPUInstance()
        {
            if (prefabManager != null && prefabManager.isActiveAndEnabled)
            {
                for (int i = 0; i < Count; i++)
                {
                    GPUInstancerAPI.DefinePrototypeVariationBuffer<Color>(prefabManager, prefabList[i].prefabPrototype, bufferName);
                }
            }
            instancesList.Clear();
            //var array = parentTransform.gameObject.GetComponentsInChildren<GPUInstancerPrefab>();
            var array = FindObjectsOfType<GPUInstancerPrefab>();
            foreach (var item in array)
            {
                var color = item.GetComponent<Renderer>().material.color;
                instancesList.Add(item);
                item.AddVariation(bufferName, color);
            }
            if (prefabManager != null && prefabManager.isActiveAndEnabled)
            {
                try
                {
                    GPUInstancerAPI.RegisterPrefabInstanceList(prefabManager, instancesList);
                    GPUInstancerAPI.InitializeGPUInstancer(prefabManager);
                }
                catch { }
            }
            RefreshColor();
        }

        public void RemoveInstances(GPUInstancerPrefab instanceCount)
        {
            //AddRemoveInstances.instance.RemoveInstances(GetComponent<GPUInstancerPrefab>());
            try
            {
                GPUInstancerAPI.RemovePrefabInstance(prefabManager, instanceCount);
            }
            catch { }
            instancesList.Remove(instanceCount);
        }

        public void RefreshColor()
        {
            foreach(var item in instancesList)
            {
                var color = item.GetComponent<Renderer>().material.color;
                GPUInstancerAPI.UpdateVariation(prefabManager, item, bufferName, color);
            }
        }
    }
}

