﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using MoreMountains.NiceVibrations;
using GPUInstancer;
using UnityEngine.EventSystems;
using GameAnalyticsSDK;

public class Controller : MonoBehaviour
{
    public static Controller instance;

    [Header("Variable")]
    public int speed;
    float fh;
    float fv;
    float h;
    float v;
    bool isDrag = false;
    Rigidbody rigid;
    Vector3 fp;
    Vector3 cp;
    int maxLevel = 100;
    bool isVibrate = true;
    public List<Texture2D> listGround = new List<Texture2D>();
    public List<Sprite> listBackground = new List<Sprite>();
    public List<GameObject> listMap = new List<GameObject>();
    public List<GameObject> listMaze = new List<GameObject>();
    public GameObject menu;
    public GameObject back;
    bool isStartGame = false;
    int ballCount = 0;
    public static int ballLeft = 100;
    public GameObject currentMaze;
    public GameObject currentMap;
    float rotLimit = 20;
    public static int currentSkin;
    public List<GameObject> listButton = new List<GameObject>();
    List<GameObject> listBall = new List<GameObject>();
    public static int state = 1;
    public List<Color> wallColors = new List<Color>();
    public GameObject bonusMap;
    bool isBonusGame = false;

    [Header("UI")]
    public GameObject winPanel;
    public GameObject losePanel;
    public Slider levelProgress1;
    public Slider levelProgress2;
    public Text currentLevelText;
    public Text nextLevelText;
    public Text moneyText;
    int currentLevel;
    public static int score;
    public static int money;
    int progress;
    public Canvas canvas;
    public static int colorCode;
    public InputField rotInput;
    public Image splashScreen;
    public List<Sprite> listBallIcon = new List<Sprite>();
    public Sprite selected;
    public Sprite unSelected;
    public GameObject shopMenu;
    public GameObject gameMenu;
    public Text bestScoreText;
    public Text bonusTimeText;
    public GameObject gemAnim;
    public GameObject shopButton;
    public GameObject bonusPopup;

    [Header("Objects")]
    public GameObject conffeti;
    public GameObject ball;
    public Transform ballManager;
    public Transform ballManager2;
    public GameObject bound;
    public GameObject plusVarPrefab;
    public GameObject bomb;
    public static GameObject bombPrefab;
    public GameObject holeBonus;
    public GameObject conffeti1;
    public GameObject conffeti2;

    private void OnEnable()
    {
        GameAnalytics.Initialize();
        //PlayerPrefs.DeleteAll();
        Application.targetFrameRate = 60;
        state = 1;
        ballLeft = 20;
        splashScreen.gameObject.SetActive(true);
        splashScreen.DOFade(0, 2f);
        Destroy(splashScreen.gameObject, 2f);
        Input.multiTouchEnabled = false;
        instance = this;
        maxLevel = listMap.Count - 1;
        currentLevel = PlayerPrefs.GetInt("currentLevel");
        currentSkin = PlayerPrefs.GetInt("currentSkin");
        listButton[currentSkin].transform.GetChild(0).GetComponent<Image>().sprite = selected;
        foreach(var item in listButton)
        {
            var id = item.name;
            var isUnlocked = PlayerPrefs.GetInt(id);
            if(isUnlocked == 1)
            {
                item.transform.GetChild(1).GetComponent<Image>().sprite = listBallIcon[int.Parse(id)];
            }
        }
        money = PlayerPrefs.GetInt("money");
        moneyText.text = money.ToString();
        score = PlayerPrefs.GetInt("bestScore");
        bestScoreText.text = score.ToString();
        currentLevelText.text = (currentLevel + 1).ToString();
        nextLevelText.text = (currentLevel + 2).ToString();
        if (currentLevel > maxLevel)
        {
            currentMap = listMap[Random.Range(0, listMap.Count)];
        }
        else
        currentMap = listMap[currentLevel];
        listMaze.Add(currentMap.transform.GetChild(0).gameObject);
        listMaze.Add(currentMap.transform.GetChild(1).gameObject);
        currentMaze = listMaze[0];
        currentMaze.transform.parent = transform;
        currentMaze.SetActive(true);
        listMaze[1].transform.DOMoveY(listMaze[1].transform.position.y - 20, 0);
        listMaze[1].transform.parent = null;
        listMaze[1].SetActive(true);
        ballManager = transform.GetChild(0).transform.GetChild(0);
        ballManager.gameObject.SetActive(true);
        ballManager2 = listMaze[1].transform.GetChild(0);
        ballManager2.gameObject.SetActive(true);
        rigid = GetComponent<Rigidbody>();
        int randomBg = 0;
        randomBg = Random.Range(0, listBackground.Count);
        var floorList = GameObject.FindGameObjectsWithTag("Floor");
        foreach(var item in floorList)
        {
            item.GetComponent<MeshRenderer>().material.mainTexture = listGround[randomBg];
        }
        back.GetComponent<SpriteRenderer>().sprite = listBackground[randomBg];
        progress = 0;
        levelProgress1.value = 0;
        levelProgress2.value = 0;
        //int randomWall = 0;
        //randomWall = Random.Range(0, wallColors.Count);
        foreach (var item in listMaze)
        {
            var temp = Instantiate(bound);
            temp.transform.parent = item.transform;
            temp.transform.localPosition = new Vector3(8, -1.6f, 8);
            temp.GetComponent<MeshRenderer>().material.color = wallColors[randomBg];
        }
        var wallList = GameObject.FindGameObjectsWithTag("Wall");
        foreach (var item in wallList)
        {
            item.GetComponent<MeshRenderer>().material.color = wallColors[randomBg];
        }
        InvokeRepeating("SpawnBall", 0, 0.001f);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, Application.version, currentLevel.ToString(currentLevel.ToString("00000")));
    }

    public void CheckTotalBalls()
    {
        if (currentLevel >= 2)
        {
            bombPrefab = Instantiate(bomb, ballManager.transform.position, Quaternion.identity);
            bombPrefab.transform.parent = ballManager.transform;
            listBall.Add(bombPrefab);
        }
        ballLeft = 20;
    }

    public void SpawnBall()
    {
        var temp = Instantiate(ball, ballManager.transform.position, Quaternion.identity);
        temp.transform.parent = ballManager.transform;
        temp.transform.localPosition = Vector3.zero;
        listBall.Add(temp);
        ballCount++;
        if (ballCount >= 20)
        {
            CancelInvoke("SpawnBall");
            ballCount = 0;
            CheckTotalBalls();
        }
    }

    private void FixedUpdate()
    {
        if (isStartGame)
        {
            if (Input.GetMouseButtonDown(0))
            {
                fh = Input.GetAxis("Mouse X") * speed;
                fv = Input.GetAxis("Mouse Y") * speed;
            }

            if (Input.GetMouseButton(0))
            {
                OnMouseDrag();
            }

            if (Input.GetMouseButtonUp(0))
            {
                isDrag = false;
            }
        }
    }

    void OnMouseDrag()
    {
#if UNITY_EDITOR
        h = Input.GetAxis("Mouse X") * speed;
        v = Input.GetAxis("Mouse Y") * speed;
        if (Mathf.Abs(h - fh) > 0.25f)
        {
            isDrag = true;
        }
        if(Mathf.Abs(v - fv) > 0.25f)
        {
            isDrag = true;
        }
#endif
#if UNITY_IOS
        if (Input.touchCount > 0)
        {
            h = Input.touches[0].deltaPosition.x/8;
            v = Input.touches[0].deltaPosition.y/8;
            isDrag = true;
        }
#endif
        if (isDrag)
        {
            var rot = new Vector3(v, 0, -h);
            transform.Rotate(rot);
            var currentRot = transform.rotation.eulerAngles;
            currentRot.x = ClampAngle(currentRot.x, -rotLimit, rotLimit);
            currentRot.y = 0;
            currentRot.z = ClampAngle(currentRot.z, -rotLimit, rotLimit);

            transform.eulerAngles = currentRot;
        }
    }

    float ClampAngle(float angle, float from, float to)
    {
        if (angle < 0f) angle = 360 + angle;
        if (angle > 180f) return Mathf.Max(angle, 360 + from);
        return Mathf.Min(angle, to);
    }

    IEnumerator delayVibrate()
    {
        yield return new WaitForSeconds(0.2f);
        isVibrate = true;
    }

    IEnumerator delayCheckBallLeft()
    {
        ballLeft--;
        Debug.Log(ballLeft);
        if ((ballLeft <= 0 || (ballManager.childCount <= 1 && bombPrefab != null)) && state == 1)
        {
            yield return new WaitForSeconds(0.2f);
            state = 2;
            Camera.main.transform.DOMoveY(Camera.main.transform.position.y - 20, 1);
            currentMaze.transform.parent = null;
            //currentMaze.transform.DOMoveX(1000, 1);
            Destroy(currentMaze);
            transform.eulerAngles = Vector3.zero;
            currentMaze = listMaze[1];
            transform.position = new Vector3(0, currentMaze.transform.position.y, 0);
            currentMaze.transform.parent = transform;
            ballManager = transform.GetChild(0).transform.GetChild(0);
            ballManager.gameObject.SetActive(true);
            if (bombPrefab != null)
            {
                bombPrefab.GetComponent<SphereCollider>().isTrigger = false;
                bombPrefab.transform.parent = Controller.instance.ballManager2.transform;
                bombPrefab.transform.localPosition = Vector3.zero;
            }
            levelProgress1 = levelProgress2;
            levelProgress1.value = 0;
            ballLeft = 20;
            progress = 0;
        }
        else if (ballLeft <= 0 || (ballManager.childCount <= 1 && bombPrefab != null))
        {
            yield return new WaitForSeconds(0.2f);
            var previousScore = PlayerPrefs.GetInt("bestScore");
            var diff = score - previousScore;
            if ((diff > 200 && currentLevel >= 2) || (diff > 250 && currentLevel >= 5) || (diff > 300 && currentLevel >= 10) || currentLevel < 2)
            {
                isBonusGame = true;
                holeBonus.transform.localPosition = new Vector3(Random.Range(0, 16), 0.21f, Random.Range(0, 16));
                var oldPos = currentMaze.transform.position;
                currentMaze.transform.parent = null;
                currentMaze.transform.DOMoveX(1000, 1);
                transform.eulerAngles = Vector3.zero;
                bonusMap.SetActive(true);
                bonusMap.transform.GetChild(0).gameObject.SetActive(true);
                currentMaze = bonusMap;
                currentMaze.transform.position = oldPos;
                transform.position = new Vector3(0, currentMaze.transform.position.y, 0);
                currentMaze.transform.parent = transform;
                ballManager = transform.GetChild(0).transform.GetChild(0);
                ballManager.gameObject.SetActive(true);
                StartCoroutine(delayBonusTimeOut());
                bonusPopup.SetActive(true);
                bonusPopup.GetComponent<RectTransform>().DOAnchorPosY(350, 0.5f);
                bonusPopup.GetComponent<RectTransform>().DOScale(0.5f, 0.5f);
            }
            else
            {
                Win();
            }
        }
    }

    public void CheckBallLeft()
    {
        StartCoroutine(delayCheckBallLeft());
    }

    IEnumerator delayBonusTimeOut()
    {
        float time = 10;
        bonusTimeText.gameObject.SetActive(true);
        while (time > 0)
        {
            time -= 0.02f;
            bonusTimeText.text = (int)time + "s";
            yield return null;
        }
        bonusTimeText.gameObject.SetActive(false);
        Win();
    }

    IEnumerator delayMoveStack()
    {
        yield return new WaitForSeconds(0.5f);
        currentMaze.transform.DOMoveX(1000, 1);
    }

    public void Scoring(int distance)
    {
        progress++;
        levelProgress1.value = progress;
        score = score + 1 + (int)(distance);
        bestScoreText.text = score.ToString();
    }

    public void PlusMoney(int plus)
    {
        money += plus;
        moneyText.text = money.ToString();
        PlayerPrefs.SetInt("money", money);
    }

    public void PlusEffect(Vector3 pos, int score)
    {
        if (!UnityEngine.iOS.Device.generation.ToString().Contains("5"))
        {
            MMVibrationManager.Vibrate();
        }
        var plusVar = Instantiate(plusVarPrefab);
        plusVar.transform.SetParent(canvas.transform);
        plusVar.transform.localScale = new Vector3(1, 1, 1);
        plusVar.transform.position = worldToUISpace(canvas, pos);
        plusVar.GetComponent<Text>().text = "+" + (int)score;
        plusVar.SetActive(true);
        plusVar.transform.DOMoveY(plusVar.transform.position.y + Random.Range(200, 300), 0.5f);
        Destroy(plusVar, 0.5f);
    }

    public Vector3 worldToUISpace(Canvas parentCanvas, Vector3 worldPos)
    {
        //Convert the world for screen point so that it can be used with ScreenPointToLocalPointInRectangle function
        Vector3 screenPos = Camera.main.WorldToScreenPoint(worldPos);
        Vector2 movePos;

        //Convert the screenpoint to ui rectangle local point
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvas.transform as RectTransform, screenPos, parentCanvas.worldCamera, out movePos);
        //Convert the local point to world point
        return parentCanvas.transform.TransformPoint(movePos);
    }

    public void Win()
    {
        if (isStartGame || isBonusGame)
        {
            SoundManager.instance.PlaySound(SoundManager.instance.win);
            gemAnim.SetActive(true);
            conffeti1.SetActive(true);
            conffeti2.SetActive(true);
            isStartGame = false;
            if (!UnityEngine.iOS.Device.generation.ToString().Contains("5"))
            {
                MMVibrationManager.Vibrate();
            }
            money += 100;
            moneyText.text = money.ToString();
            PlayerPrefs.SetInt("money", money);
            PlayerPrefs.SetInt("bestScore", score);
            StartCoroutine(delayEnd(winPanel, 0.1f));
            currentLevel++;
            PlayerPrefs.SetInt("currentLevel", currentLevel);
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, Application.version, currentLevel.ToString("00000"), score);
        }
    }

    public void Lose()
    {
        if (isStartGame)
        {
            isStartGame = false;
            StartCoroutine(delayEnd(losePanel, 1));
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, Application.version, currentLevel.ToString("00000"), score);
        }
    }

    IEnumerator delayEnd(GameObject panel, float time)
    {
        yield return new WaitForSeconds(time);
        panel.SetActive(true);
        var random = Random.Range(0, 100);
        if (panel != losePanel && random > 50)
        {
            panel.transform.GetChild(0).GetComponent<Text>().text = "SUPER";
            panel.transform.GetChild(0).GetComponent<Text>().color = Color.red;
        }
    }

    public void StartGameButton()
    {
        menu.SetActive(false);
        isStartGame = true;
    }

    public void ReplayButton()
    {
        SceneManager.LoadScene(0);
    }

    public void NextMap()
    {
        isStartGame = false;
        winPanel.SetActive(true);
        currentLevel++;
        if (currentLevel > maxLevel)
        {
            currentLevel = 0;
        }
        PlayerPrefs.SetInt("currentLevel", currentLevel);
        SceneManager.LoadScene(0);
    }

    IEnumerator delayLoadScene()
    {
        yield return new WaitForSeconds(0.1f);
        SceneManager.LoadScene(0);
    }

    public void BtnNextMap()
    {
        NextMap();
    }

    public void BtnPreviousMap()
    {
        isStartGame = false;
        winPanel.SetActive(true);
        currentLevel--;
        if (currentLevel < 0)
        {
            currentLevel = maxLevel;
        }
        PlayerPrefs.SetInt("currentLevel", currentLevel);
        SceneManager.LoadScene(0);
    }

    public void ChooseSkinButton()
    {
        var currentSkinButton = EventSystem.current.currentSelectedGameObject;
        var currentSkin = int.Parse(currentSkinButton.name);
        var id = currentSkinButton.name;
        var isUnlocked = PlayerPrefs.GetInt(id);
        if (isUnlocked == 1)
        {
            foreach (var item in listButton)
            {
                if (int.Parse(item.name) == currentSkin)
                {
                    PlayerPrefs.SetInt("currentSkin", currentSkin);
                    item.transform.GetChild(0).GetComponent<Image>().sprite = selected;
                }
                else
                {
                    item.transform.GetChild(0).GetComponent<Image>().sprite = unSelected;
                }
            }
        }
    }

    public void UnlockRandomSkin()
    {
        List<GameObject> listToUnlock = new List<GameObject>();
        foreach (var item in listButton)
        {
            var id = item.name;
            var isUnlocked = PlayerPrefs.GetInt(id);
            if (isUnlocked == 0)
            {
                listToUnlock.Add(item);
            }
        }
        if (money >= 1000 && listToUnlock.Count > 0)
        {
            var random = Random.Range(0, listToUnlock.Count);
            var id = listToUnlock[random].name;
            listToUnlock[random].transform.GetChild(1).GetComponent<Image>().sprite = listBallIcon[int.Parse(id)];
            PlayerPrefs.SetInt(id, 1);
            money -= 1000;
            moneyText.text = money.ToString();
            PlayerPrefs.SetInt("money", money);
            SoundManager.instance.PlaySound(SoundManager.instance.win);
        }
    }

    public void ButtonOpenShop()
    {
        menu.SetActive(false);
        isStartGame = false;
        currentMaze.SetActive(false);
        listMaze[1].SetActive(false);
        shopMenu.SetActive(true);
        gameMenu.SetActive(false);
        shopButton.SetActive(false);
    }

    public void ButtonCloseShop()
    {
        menu.SetActive(true);
        currentMaze.SetActive(true);
        listMaze[1].SetActive(true);
        shopMenu.SetActive(false);
        gameMenu.SetActive(true);
        shopButton.SetActive(true);
    }

    public void OnFillChanged()
    {
        levelProgress1.transform.GetChild(2).transform.GetChild(0).GetComponent<Image>().DOFade(1, 0.2f);
    }
}