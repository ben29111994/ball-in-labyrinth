﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using MoreMountains.NiceVibrations;

public class Ball : MonoBehaviour
{
    public List<GameObject> prefabList = new List<GameObject>();
    public GameObject coinEffect;

    void Start()
    {
        var temp = Instantiate(prefabList[Controller.currentSkin], transform);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Coin")
        {
            Controller.instance.PlusMoney(1);
            other.transform.DOMoveZ(other.transform.position.z + 2, 0.25f);
            var coinPrefab = Instantiate(coinEffect);
            coinPrefab.transform.parent = Controller.instance.canvas.transform;
            coinPrefab.transform.SetAsFirstSibling();
            coinPrefab.transform.localScale = Vector3.zero;
            coinPrefab.transform.position = worldToUISpace(Controller.instance.canvas, transform.position);
            coinPrefab.SetActive(true);
            if (!UnityEngine.iOS.Device.generation.ToString().Contains("5"))
            {
                MMVibrationManager.Vibrate();
            }
            Destroy(coinPrefab, 2);
            Destroy(other.gameObject, 0.25f);
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if(collision.gameObject.tag == "Roof")
        {
            Controller.instance.CheckBallLeft();
            Destroy(gameObject);
        }
    }


    public Vector3 worldToUISpace(Canvas parentCanvas, Vector3 worldPos)
    {
        //Convert the world for screen point so that it can be used with ScreenPointToLocalPointInRectangle function
        Vector3 screenPos = Camera.main.WorldToScreenPoint(worldPos);
        Vector2 movePos;

        //Convert the screenpoint to ui rectangle local point
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvas.transform as RectTransform, screenPos, parentCanvas.worldCamera, out movePos);
        //Convert the local point to world point
        return parentCanvas.transform.TransformPoint(movePos);
    }
}
